import logging
import requests

logging.basicConfig(level=logging.ERROR)


def call(method, url=None, endpoint=None, headers=None, payload=None, timeout=5, **request_kwargs):
    """
    Make an HTTP request to the Pokémon API.

    :param url: API endpoint URL
    :param method: HTTP method (GET, POST, etc.)
    :param endpoint: API endpoint URL suffix
    :param headers: Optional headers to include in the request
    :param payload: Optional payload data (JSON by default)
    :param timeout: Request timeout in seconds
    :param request_kwargs: Additional keyword arguments to pass to requests library
    :return: Response object
    """
    try:
        if url:
            full_url = url
        elif endpoint:
            full_url = 'https://pokeapi.co/api/v2/' + endpoint
        else:
            raise ValueError("Either 'url' or 'endpoint' must be provided.")

        action = getattr(requests, method.lower())
        response = action(url=full_url, headers=headers, json=payload, timeout=timeout, **request_kwargs)
        response.raise_for_status()
        return response
    except ValueError as ve:
        logging.error(f"Invalid request: {ve}")
        return None
    except requests.RequestException as e:
        logging.error(f"Error making {method} request to {full_url}: {e}")
        return None
