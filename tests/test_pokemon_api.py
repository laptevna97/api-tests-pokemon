import pytest
from api.pokemon_api_client import call


@pytest.fixture(scope="module")
def pokemon_types_response():
    response = call('GET', endpoint='type', headers={'Content-Type': 'application/json'})
    return response.json()


def test_pokemon_types_response(pokemon_types_response):
    assert pokemon_types_response is not None

    assert isinstance(pokemon_types_response, dict)

    assert len(pokemon_types_response['results']) == 20


def test_fire_type_pokemon():
    response = call('GET', endpoint='type/fire', headers={'Content-Type': 'application/json'})
    fire_pokemon = response.json()['pokemon']

    charmander_in_list = any(pokemon['pokemon']['name'] == 'charmander' for pokemon in fire_pokemon)
    bulbasaur_not_in_list = all(pokemon['pokemon']['name'] != 'bulbasaur' for pokemon in fire_pokemon)

    assert charmander_in_list
    assert bulbasaur_not_in_list


def test_heaviest_fire_pokemon():
    expected_weights = {
        'charizard-gmax': 10000,
        'cinderace-gmax': 10000,
        'coalossal-gmax': 10000,
        'centiskorch-gmax': 10000,
        'groudon-primal': 9997
    }

    response = call('GET', endpoint='type/fire', headers={'Content-Type': 'application/json'})
    fire_pokemon = response.json()['pokemon']

    for pokemon in fire_pokemon:
        name = pokemon['pokemon']['name']
        if name in expected_weights:

            url = pokemon['pokemon']['url']
            weight_response = call('GET', url=url, headers={'Content-Type': 'application/json'})
            weight = weight_response.json()['weight']
            assert weight == expected_weights[name]
