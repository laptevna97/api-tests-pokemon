# Pokémon API Test Automation



This project contains automated tests written in Python using the `pytest` framework to interact with the Pokémon API (https://pokeapi.co/api/v2/). The tests cover various scenarios outlined in the assignment provided.

## Installation

1. Clone this repository to your local machine.

2. Install the required dependencies using pip:

```
pip install -r requirements.txt
```

## Usage

1. Ensure that Python and `pytest` are installed on your machine.
2. Navigate to the project directory.
3. Run the tests using the following command:

```
pytest
```
### Running Individual Tests

To run specific tests, use the `-k` option followed by the test name pattern. For example, to run the test for verifying the Pokémon type API response, use:
```
pytest -k test_pokemon_types_response
```
Similarly, to run other individual tests, replace `test_pokemon_types_response` with the name of the desired test function.

## Project Structure

- **tests**: Contains the test cases written using `pytest`.
- **api**: Contains the `pokemon_api_client.py` file, which defines the `call` function to interact with the Pokémon API.
- **requirements.txt**: Contains a list of dependencies required to run the project.

## Test Scenarios

1. **test_pokemon_types_response**: Validates the response from the Pokémon type API, ensuring it returns a JSON response with exactly 20 different Pokémon types.

2. **test_fire_type_pokemon**: Verifies that the "Fire type" Pokémon list contains the Pokémon "charmander" and does not contain "bulbasaur".

3. **test_heaviest_fire_pokemon**: Identifies the five heaviest Pokémon of the Fire type and ensures they have the expected weights.

## Additional Details

- Error Handling: The `call` function in `pokemon_api_client.py` includes error handling to handle requests exceptions gracefully.
- Assumptions: No assumptions were made during the implementation. All scenarios were addressed as per the provided requirements.

## Author

laptevna97@gmail.com